package com.sda.javagda14.dp.singleton;

public class Manager {

    // forma 1
//    private static final Manager manager = new Manager();
    // forma 2
    private static Manager manager = new Manager();
    private Manager() {
    }


    // forma 3
    // lazy singleton
//    private static final Object lock = new Object();
//    public synchronized static Manager getManager() {
//        if (manager == null) {
//            synchronized (lock) {
//                if (manager == null) {
//                    manager = new Manager();
//                }
//            }
//        }
//        return manager;
//    }

    public void wypisz() {
        System.out.println("Cośtam");
    }
}
