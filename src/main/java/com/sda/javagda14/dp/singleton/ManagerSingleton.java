package com.sda.javagda14.dp.singleton;

// eager
public enum ManagerSingleton {
    INSTANCE;

    public void metoda(){
        System.out.println("Wypisz cos - coś");
    }

}
