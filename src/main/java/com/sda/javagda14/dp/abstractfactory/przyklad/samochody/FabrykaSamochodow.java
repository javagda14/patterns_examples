package com.sda.javagda14.dp.abstractfactory.przyklad.samochody;

// abstract factory
public abstract class FabrykaSamochodow {

    // metoda factory
    public static Samochod stworzBMWe36() {
        return new Samochod("BMW", 1.6, false, 112, "e46");
    }

}
