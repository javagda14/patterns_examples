package com.sda.javagda14.dp.abstractfactory.przyklad.samochody;

public interface ISamochod {
    public String getProducent();

    public void setProducent(String producent);

    public double getPojemnosc();

    public void setPojemnosc(double pojemnosc);

    public boolean isPosiadaTurbine();

    public void setPosiadaTurbine(boolean posiadaTurbine);

    public int getMoc();

    public void setMoc(int moc);

    public String getModel();

    public void setModel(String model);
}
