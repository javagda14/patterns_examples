package com.sda.javagda14.dp.abstractfactory.przyklad.samochody;

class Samochod implements ISamochod{
    private String producent;
    private double pojemnosc;
    private boolean posiadaTurbine;
    private int moc;
    private String model;

    Samochod() {
    }

    Samochod(String producent, double pojemnosc, boolean posiadaTurbine, int moc, String model) {
        this.producent = producent;
        this.pojemnosc = pojemnosc;
        this.posiadaTurbine = posiadaTurbine;
        this.moc = moc;
        this.model = model;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public double getPojemnosc() {
        return pojemnosc;
    }

    public void setPojemnosc(double pojemnosc) {
        this.pojemnosc = pojemnosc;
    }

    public boolean isPosiadaTurbine() {
        return posiadaTurbine;
    }

    public void setPosiadaTurbine(boolean posiadaTurbine) {
        this.posiadaTurbine = posiadaTurbine;
    }

    public int getMoc() {
        return moc;
    }

    public void setMoc(int moc) {
        this.moc = moc;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    public static Samochod stworzMalucha() {
        return new Samochod("Fiat", 2.0, true, 300, "126p");
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "producent='" + producent + '\'' +
                ", pojemnosc=" + pojemnosc +
                ", posiadaTurbine=" + posiadaTurbine +
                ", moc=" + moc +
                ", model='" + model + '\'' +
                '}';
    }
}
