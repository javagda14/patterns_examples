package com.sda.javagda14.dp.abstractfactory.przyklad;

import com.sda.javagda14.dp.abstractfactory.przyklad.samochody.FabrykaSamochodow;
import com.sda.javagda14.dp.abstractfactory.przyklad.samochody.ISamochod;
//import com.sda.javagda14.dp.abstractfactory.przyklad.samochody.Samochod;


public class Main {
    public static void main(String[] args) {
//        ISamochod samochod = new Samochod("Volvo", 30.0, true, 10000000, "Mój");

        ISamochod samochodBmw = FabrykaSamochodow.stworzBMWe36();
//        ISamochod jakis = FabrykaSamochodow.stworzMalucha();

        // nie moge stworzyć obiektu klasy Samochod.
//        ISamochod samochod = Samochod.stworzMalucha();
        System.out.println(samochodBmw);
    }
}
