package com.sda.javagda14.dp.observer.przyklad.own;

public class Oferta {
    private String co;
    private double cena;

    public Oferta(String co, double cena) {
        this.co = co;
        this.cena = cena;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }
}
