package com.sda.javagda14.dp.observer.przyklad.own;

public class Uzytkownik implements Powiadamialny{
    private String imie;

    public Uzytkownik(String imie) {
        this.imie = imie;
    }

    @Override
    public void powiadom(Object oferta) {
        if (oferta instanceof Oferta) {
            Oferta jakasOferta = (Oferta) oferta;
            System.out.println("Otrzymałem(" + imie + ") oferte: " + ((Oferta) oferta).getCo());
        }else{
            System.out.println(imie + " powiadomiony o: " + oferta);
        }
    }
}
