package com.sda.javagda14.dp.observer.przyklad.lib;

public class Main {
    public static void main(String[] args) {
        Portal portal = new Portal();

        portal.addObserver(new Uzytkownik("Marian"));
        portal.addObserver(new Uzytkownik("Dorian"));
        portal.addObserver(new Uzytkownik("Szczepan"));

        portal.powiadom("Bułki podrożały");
    }
}
