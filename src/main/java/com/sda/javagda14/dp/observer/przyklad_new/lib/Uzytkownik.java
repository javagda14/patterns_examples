package com.sda.javagda14.dp.observer.przyklad_new.lib;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class Uzytkownik implements /*ChangeListener<String>, */ChangeListener<Wiadomosc> {
    private String imie;

    public Uzytkownik(String imie) {
        this.imie = imie;
    }
/*

    @Override
    public void changed(ObservableValue<? extends String> observable,
                        String oldValue,
                        String newValue) {
        System.out.println("(" + imie + ") Jestem powiadomiony o: " + newValue);
    }
*/

    @Override
    public void changed(ObservableValue<? extends Wiadomosc> observable,
                        Wiadomosc oldValue,
                        Wiadomosc newValue) {
        System.out.println("(" + imie + ") Jestem powiadomiony o: " + newValue);
    }
}
