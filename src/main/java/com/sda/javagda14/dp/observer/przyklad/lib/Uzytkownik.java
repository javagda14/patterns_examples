package com.sda.javagda14.dp.observer.przyklad.lib;

import java.util.Observable;
import java.util.Observer;

public class Uzytkownik implements Observer {

    private String imie;

    public Uzytkownik(String imie) {
        this.imie = imie;
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println(
                imie + " powiadomiony o : " + arg);
    }
}
