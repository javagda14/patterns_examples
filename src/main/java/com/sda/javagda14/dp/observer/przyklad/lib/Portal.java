package com.sda.javagda14.dp.observer.przyklad.lib;

import java.util.Observable;

public class Portal extends Observable {

    public void powiadom(String wiadomosc) {
        // ustawiamy flagę "zmieniłem się" (jako obiekt)
        setChanged();
        // rozsyłamy wiadomość
        notifyObservers(wiadomosc);
    }
}
