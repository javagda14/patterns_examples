package com.sda.javagda14.dp.observer.przyklad.own;

public class Main {
    public static void main(String[] args) {
        Portal allegro = new Portal();

        allegro.dodajSubskrybenta(new Uzytkownik("Marian"));
        allegro.dodajSubskrybenta(new Uzytkownik("Szczepan"));
        allegro.dodajSubskrybenta(new Uzytkownik("Michał"));

        allegro.wyslijOferte("Nowy laptop za pół ceny.");

        allegro.wyslijOferte(new Oferta("Bułki", 2.30));
    }
}
