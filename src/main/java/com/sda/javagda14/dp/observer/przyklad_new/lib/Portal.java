package com.sda.javagda14.dp.observer.przyklad_new.lib;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;

public class Portal {

//    private StringProperty newsProperty = new SimpleStringProperty();
    private ObjectProperty<Wiadomosc> wiadomoscObjectProperty = new SimpleObjectProperty<>();

    //    public void addObserver(ChangeListener<String> stringChangeListener){
    public void addObserver(ChangeListener<Wiadomosc> stringChangeListener) {
//        newsProperty.addListener(stringChangeListener);
        wiadomoscObjectProperty.addListener(stringChangeListener);
    }

//    public void wyslijWiadomosc(String wiadomosc) {
    public void wyslijWiadomosc(Wiadomosc wiadomosc) {
//        newsProperty.setValue(wiadomosc);
        wiadomoscObjectProperty.setValue(wiadomosc);
    }

}
