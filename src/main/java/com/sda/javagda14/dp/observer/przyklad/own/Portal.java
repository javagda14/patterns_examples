package com.sda.javagda14.dp.observer.przyklad.own;

import java.util.ArrayList;
import java.util.List;

public class Portal {
    // observable

    private List<Powiadamialny> uzytkownikList = new ArrayList<>();

    public void dodajSubskrybenta(Powiadamialny uzytkownik) {
        uzytkownikList.add(uzytkownik);
    }

    public void wyslijOferte(Object oferta){
        for (Powiadamialny user : uzytkownikList) {
            user.powiadom(oferta);
        }
    }

}
