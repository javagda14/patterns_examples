package com.sda.javagda14.dp.builder;

import java.util.ArrayList;
import java.util.List;

public class Main {
//    public static void main(String[] args) {
//        Hero hero1 = new Hero.Builder()
//                .setName("Marian")
//                .setHealth(200)
//                .setAttackPoints(200)
//                .setDefncePoints(50)
//                .setShoeNumber(15.0)
//                .setPetsName("Mruczek")
//                .setLikesToDrink(true)
//                .createHero();
//
//        Hero hero2 = new Hero.Builder()
//                .setName("Marian")
//                .setHealth(200)
//                .setAttackPoints(200)
//                .setDefncePoints(50)
//                .setShoeNumber(15.0)
//                .setPetsName("Mruczek")
//                .setLikesToDrink(true)
//                .createHero();
//
//
//        hero1.setHealth(150);
//
//    }

    public static void main(String[] args) {
        Hero.Builder heroBuilder = new Hero.Builder()
                .setName("Marian")
                .setHealth(200)
                .setAttackPoints(200)
                .setDefncePoints(50)
                .setShoeNumber(15.0)
                .setPetsName("Mruczek")
                .setLikesToDrink(true);

        Hero hero1 = heroBuilder.createHero();
        heroBuilder.setName("Maniek");

        Hero hero2 = heroBuilder.createHero();

        System.out.println(hero1);
        System.out.println(hero2);

        List<Hero> list = new ArrayList<Hero>();
        list.add(hero1);
        list.add(hero2);

        System.out.println(list);
    }
}
