package com.sda.javagda14.dp.builder;

public class Hero {
    private String name, surname, nick, mothersName, fathersName, petsName;
    private int health, mana, attackPoints, defncePoints, attackPointsWithBow, attackPointsWithSword, defncePointsWithBow, defncePointsWithSword;
    private double shoeNumber, height;
    private boolean likesFood, likesToDrink, isDrunk;

    public Hero(String name, String surname, String nick, String mothersName, String fathersName, String petsName, int health, int mana, int attackPoints, int defncePoints, int attackPointsWithBow, int attackPointsWithSword, int defncePointsWithBow, int defncePointsWithSword, double shoeNumber, double height, boolean likesFood, boolean likesToDrink, boolean isDrunk) {
        this.name = name;
        this.surname = surname;
        this.nick = nick;
        this.mothersName = mothersName;
        this.fathersName = fathersName;
        this.petsName = petsName;
        this.health = health;
        this.mana = mana;
        this.attackPoints = attackPoints;
        this.defncePoints = defncePoints;
        this.attackPointsWithBow = attackPointsWithBow;
        this.attackPointsWithSword = attackPointsWithSword;
        this.defncePointsWithBow = defncePointsWithBow;
        this.defncePointsWithSword = defncePointsWithSword;
        this.shoeNumber = shoeNumber;
        this.height = height;
        this.likesFood = likesFood;
        this.likesToDrink = likesToDrink;
        this.isDrunk = isDrunk;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", nick='" + nick + '\'' +
                ", mothersName='" + mothersName + '\'' +
                ", fathersName='" + fathersName + '\'' +
                ", petsName='" + petsName + '\'' +
                ", health=" + health +
                ", mana=" + mana +
                ", attackPoints=" + attackPoints +
                ", defncePoints=" + defncePoints +
                ", attackPointsWithBow=" + attackPointsWithBow +
                ", attackPointsWithSword=" + attackPointsWithSword +
                ", defncePointsWithBow=" + defncePointsWithBow +
                ", defncePointsWithSword=" + defncePointsWithSword +
                ", shoeNumber=" + shoeNumber +
                ", height=" + height +
                ", likesFood=" + likesFood +
                ", likesToDrink=" + likesToDrink +
                ", isDrunk=" + isDrunk +
                '}';
    }

    public static class Builder {
        private String name = "Marian";
        private String surname;
        private String nick;
        private String mothersName;
        private String fathersName;
        private String petsName;
        private int health;
        private int mana;
        private int attackPoints;
        private int defncePoints;
        private int attackPointsWithBow;
        private int attackPointsWithSword;
        private int defncePointsWithBow;
        private int defncePointsWithSword;
        private double shoeNumber;
        private double height;
        private boolean likesFood;
        private boolean likesToDrink;
        private boolean isDrunk;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder setNick(String nick) {
            this.nick = nick;
            return this;
        }

        public Builder setMothersName(String mothersName) {
            this.mothersName = mothersName;
            return this;
        }

        public Builder setFathersName(String fathersName) {
            this.fathersName = fathersName;
            return this;
        }

        public Builder setPetsName(String petsName) {
            this.petsName = petsName;
            return this;
        }

        public Builder setHealth(int health) {
            this.health = health;
            return this;
        }

        public Builder setMana(int mana) {
            this.mana = mana;
            return this;
        }

        public Builder setAttackPoints(int attackPoints) {
            this.attackPoints = attackPoints;
            return this;
        }

        public Builder setDefncePoints(int defncePoints) {
            this.defncePoints = defncePoints;
            return this;
        }

        public Builder setAttackPointsWithBow(int attackPointsWithBow) {
            this.attackPointsWithBow = attackPointsWithBow;
            return this;
        }

        public Builder setAttackPointsWithSword(int attackPointsWithSword) {
            this.attackPointsWithSword = attackPointsWithSword;
            return this;
        }

        public Builder setDefncePointsWithBow(int defncePointsWithBow) {
            this.defncePointsWithBow = defncePointsWithBow;
            return this;
        }

        public Builder setDefncePointsWithSword(int defncePointsWithSword) {
            this.defncePointsWithSword = defncePointsWithSword;
            return this;
        }

        public Builder setShoeNumber(double shoeNumber) {
            this.shoeNumber = shoeNumber;
            return this;
        }

        public Builder setHeight(double height) {
            this.height = height;
            return this;
        }

        public Builder setLikesFood(boolean likesFood) {
            this.likesFood = likesFood;
            return this;
        }

        public Builder setLikesToDrink(boolean likesToDrink) {
            this.likesToDrink = likesToDrink;
            return this;
        }

        public Builder setIsDrunk(boolean isDrunk) {
            this.isDrunk = isDrunk;
            return this;
        }

        public Hero createHero() {
            return new Hero(name, surname, nick, mothersName, fathersName, petsName, health, mana, attackPoints, defncePoints, attackPointsWithBow, attackPointsWithSword, defncePointsWithBow, defncePointsWithSword, shoeNumber, height, likesFood, likesToDrink, isDrunk);
        }
    }
}
